import request from '@/utils/request'

// 查询【请填写功能名称】列表
export function listConsultation(query) {
  return request({
    url: '/system/consultation/list',
    method: 'get',
    params: query
  })
}

// 查询【请填写功能名称】详细
export function getConsultation(id) {
  return request({
    url: '/system/consultation/' + id,
    method: 'get'
  })
}

// 新增【请填写功能名称】
export function addConsultation(data) {
  return request({
    url: '/system/consultation',
    method: 'post',
    data: data
  })
}

// 修改【请填写功能名称】
export function updateConsultation(data) {
  return request({
    url: '/system/consultation',
    method: 'put',
    data: data
  })
}

// 删除【请填写功能名称】
export function delConsultation(id) {
  return request({
    url: '/system/consultation/' + id,
    method: 'delete'
  })
}
