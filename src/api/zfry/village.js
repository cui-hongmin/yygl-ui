import request from '@/utils/request'

// 查询【请填写功能名称】列表
export function listVillage(query) {
  return request({
    url: '/system/village/list',
    method: 'get',
    params: query
  })
}

// 查询【请填写功能名称】详细
export function getVillage(id) {
  return request({
    url: '/system/village/' + id,
    method: 'get'
  })
}

// 新增【请填写功能名称】
export function addVillage(data) {
  return request({
    url: '/system/village',
    method: 'post',
    data: data
  })
}

// 修改【请填写功能名称】
export function updateVillage(data) {
  return request({
    url: '/system/village',
    method: 'put',
    data: data
  })
}

// 删除【请填写功能名称】
export function delVillage(id) {
  return request({
    url: '/system/village/' + id,
    method: 'delete'
  })
}
