import request from '@/utils/request'

// 查询【请填写功能名称】列表
export function listZx(query) {
  return request({
    url: '/system/zx/list',
    method: 'get',
    params: query
  })
}

// 查询【请填写功能名称】详细
export function getZx(id) {
  return request({
    url: '/system/zx/' + id,
    method: 'get'
  })
}

// 新增【请填写功能名称】
export function addZx(data) {
  return request({
    url: '/system/zx',
    method: 'post',
    data: data
  })
}

// 修改【请填写功能名称】
export function updateZx(data) {
  return request({
    url: '/system/zx',
    method: 'put',
    data: data
  })
}

// 删除【请填写功能名称】
export function delZx(id) {
  return request({
    url: '/system/zx/' + id,
    method: 'delete'
  })
}
