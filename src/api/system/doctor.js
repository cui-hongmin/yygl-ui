import request from '@/utils/request'

// 查询【请填写功能名称】列表
export function listDoctor(query) {
  return request({
    url: '/system/doctor/list',
    method: 'get',
    params: query
  })
}

// 查询【请填写功能名称】详细
export function getDoctor(id) {
  return request({
    url: '/system/doctor/' + id,
    method: 'get'
  })
}

// 新增【请填写功能名称】
export function addDoctor(data) {
  return request({
    url: '/system/doctor',
    method: 'post',
    data: data
  })
}

// 修改【请填写功能名称】
export function updateDoctor(data) {
  return request({
    url: '/system/doctor',
    method: 'put',
    data: data
  })
}

// 删除【请填写功能名称】
export function delDoctor(id) {
  return request({
    url: '/system/doctor/' + id,
    method: 'delete'
  })
}
