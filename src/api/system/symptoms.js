import request from '@/utils/request'

// 查询线上诊断列表
export function listSymptoms(query) {
  return request({
    url: '/system/symptoms/list',
    method: 'get',
    params: query
  })
}

// 查询【根据id查询需诊断】详细
export function getSymptoms(id) {
  return request({
    url: '/system/symptoms/' + id,
    method: 'get'
  })
}

// 修改【请填写功能名称】
export function updateSymptoms(data) {
  return request({
    url: '/system/symptoms',
    method: 'put',
    data: data
  })
}