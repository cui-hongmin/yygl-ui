import request from '@/utils/request'

// 查询【请填写功能名称】列表
export function listInfusion(query) {
  return request({
    url: '/system/infusion/list',
    method: 'get',
    params: query
  })
}

// 查询【请填写功能名称】详细
export function getInfusion(id) {
  return request({
    url: '/system/infusion/' + id,
    method: 'get'
  })
}

// 新增【请填写功能名称】
export function addInfusion(data) {
  return request({
    url: '/system/infusion',
    method: 'post',
    data: data
  })
}

// 修改【请填写功能名称】
export function updateInfusion(data) {
  return request({
    url: '/system/infusion',
    method: 'put',
    data: data
  })
}

// 删除【请填写功能名称】
export function delInfusion(id) {
  return request({
    url: '/system/infusion/' + id,
    method: 'delete'
  })
}
